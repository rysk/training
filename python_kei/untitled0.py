# -*- coding: utf-8 -*-
"""
Created on Sun Aug 04 13:43:32 2013

@author: kei
"""
import matplotlib.mlab as mlb
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
import copy
from datetime import date
import gc
import time
gc.enable()


time0 = time.clock() 

#date = date.today().strftime('%Y_%m_%d')
class_file = '/Users/rysk/git/python_kei/class_data.txt'
tmg_file = '/Users/rysk/git/python_kei/tmgD.txt'
pre1 = np.loadtxt(class_file, dtype='float', delimiter='\t', unpack=True, skiprows=3)#ファイル確認用
pre2 = np.loadtxt(tmg_file, dtype='int', delimiter='\t', unpack=True)#ファイル確認用
Time,spk_ID1,unit = np.loadtxt(class_file, usecols=(0,1,2),dtype='float', delimiter='\t', unpack=True, skiprows=3)
tri_num,pict,pict_st,pict_fin = np.loadtxt(tmg_file,dtype='int', delimiter='\t', unpack=True)
#Time：発火時刻　unit：発火した細胞 tri_num：刺激呈示回数 pict_num：刺激番号 pict_st：刺激呈示時刻（スパイク番号） pict_fin：刺激呈示終了時刻（スパイク番号）
#spk_ID1はクシ型の電極のときに使う　普段は使わない
samp_rate = 20000. #サンプリングレート pict_stなどを小数にするために「.」をつける
pict_num = 65 #刺激の枚数
all_trial = 25
width_befor = -0.1
width_after = 0.5
pict -= 1
bins_num = 60
bin_width = (width_after - width_befor)/bins_num

pict_st = pict_st/samp_rate #スパイク番号から時刻に変換
pict_fin = pict_fin/samp_rate #スパイク番号から時刻に変換
trial = np.array(tri_num/pict_num,dtype=int) #通し番号からトライアル番号に変換 0~24
trial_id = np.array(np.unique(trial),dtype='int')
unit_id = np.array(np.unique(unit),dtype='int')  # extract units ids
pict_array = pict.T.reshape(all_trial,pict_num)#縦がトライアル、横が刺激番号の行列を作成
tmg_array = pict_st.T.reshape(all_trial,pict_num)
#細胞番号、トライアル番号、刺激番号　　0～

def renum(unit_id):#とびとびの細胞の番号を連続に直す
    unit_num = np.empty_like(unit)
    for i,n in enumerate(unit_id):
        unit_num[mlb.find(unit == n)] = i 
    return np.array(unit_num,dtype=int)

unit_num = renum(unit_id)


time1 = time.clock() 

print time1-time0
#細胞毎の発火タイミングに分ける　
#spk_raw[n]で細胞番号nの発火タイミング(時間はぶっ通し)


#==============================================================================
#==============================================================================
#spk_raw=[] 
#for n in np.unique(unit_num):
#    spk_n_raw = []    
#    for m in mlb.find(unit_num == n):
#            spk_n_raw.append(Time[m])
#    spk_raw.append(spk_n_raw)


#==============================================================================
#==============================================================================

spk_raw=[] 
a = len(np.unique(unit_num))
for n in np.unique(unit_num):
    b = len(mlb.find(unit_num == n))
    spk_n_raw = np.empty(b)
    o = 0
    for m in mlb.find(unit_num == n):
        spk_n_raw[o] = Time[m]
        o += 1
    spk_raw.append(spk_n_raw)

#spk_raw=np.array(spk_raw)
#spk_raw=np.array(spk_raw)
#
#trial_pict_tmg_array[トライアル番号][刺激番号]=提示時刻
trial_pict_tmg_array = np.zeros((all_trial,pict_num))
for n in np.arange(all_trial):
    for m in np.arange(pict_num):
        trial_pict_tmg_array[n][pict_array[n][m]] = tmg_array[n][m]
time2 = time.clock() 
print time2-time1

#psth_pre[細胞番号（0～）][trial番号（0～）][刺激番号（0～）]
psth_pre=[]

for n in np.arange(len(unit_id)):
    psth_pre_cell = []
    for m in np.arange(all_trial):
        psth_pre_trial = []
        for o in np.arange(pict_num):
            p = spk_raw[n] - trial_pict_tmg_array[m][o]
            q = copy.deepcopy(p[(width_befor <= p) & (p <= width_after)])
            psth_pre_trial.append(q)
        psth_pre_cell.append(psth_pre_trial)
    psth_pre.append(psth_pre_cell)
time3 = time.clock() 
print time3-time2


#psth[細胞番号（0～）][刺激番号]
psth=[]
for n in np.arange(len(unit_id)):
    psth_cell = []
    for o in np.arange(pict_num):
        psth_pict=[]
        for m in np.arange(all_trial):
            psth_trial = copy.deepcopy(psth_pre[n][m][o])
            psth_pict.extend(psth_trial)
        psth_cell.append(psth_pict)
    psth.append(psth_cell)


#下のループで[]を消すのだが、psth[n][o]=[]の場合その列ごと消えてしまうので、それを防ぐためにこの場合のみ適当な値を入れる。
#histのrangeで範囲を限定するので-5は影響を与えない。
for n in np.arange(len(unit_id)):
    for o in np.arange(pict_num):
        if psth[n][o] == []:
           psth[n][o] = [-5] 

#[]があるとhistできないので[]を取り除く
for n in np.arange(len(unit_id)):
    while [] in psth[n]:
        psth[n].remove([])


time4 = time.clock()
print time4-time3
print time4-time0



#left = copy.deepcopy(np.histogram(psth[n][o],bins_num,range=(width_befor,width_after))[1][:-1])

hei=[]
for n in np.arange(len(unit_id)):
    Hei=[]
    for o in np.arange(pict_num):
        height = copy.deepcopy(np.histogram(psth[n][o],bins=bins_num,range=(width_befor,width_after))[0])*1.0       
        height = height / all_trial / bin_width
        Hei.append(height)
    hei.append(Hei)

left = np.arange(width_befor,width_after,bin_width)

time5 = time.clock()
print time5-time4
print 'start'


for n in np.arange(len(unit_id)):
    ymax=int(np.max(hei[n]))+1
    img =  'cell'+str(n+1)
    plt.figure(img)
    plt.title("cell"+str(n+1), fontsize=25, fontname='serif') # タイトル
    for o in np.arange(pict_num):
        plt.subplot(8,9,o+1)
        plt.bar(left,hei[n][o],bin_width)
#        plt.xlabel(str(o))
        plt.xticks(np.arange(width_befor,width_after*1.01,(width_after - width_befor)/2))        
        plt.yticks((0,ymax))
        plt.ylim(0,ymax)
        plt.xlim(width_befor,width_after)
    # plt.show()
    plt.savefig(img)
    plt.clf()
    plt.close()
    print n

time6 = time.clock()
print time6-time5
print time6-time0
print 'fin'
