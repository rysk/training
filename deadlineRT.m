function [hh mm ss] = deadlineRT(yourgrade)
%%
deadline_M = [2014 2 15 15 00 00.0000];
deadline_B = [2014 2 8 15 00 00.0000];
deadline_DC1 = [2014 4 31 15 00 00.0000];

dateNow=clock;
if strcmp(yourgrade,'B4')
    restTime=etime(deadline_B, dateNow);

elseif strcmp(yourgrade,'M2')
    restTime=etime(deadline_M, dateNow);
elseif strcmp(yourgrade,'DC1')
     restTime=etime(deadline_DC1, dateNow);
end

hh=restTime/(3600) -mod(restTime/(3600),1);
mm=(restTime-hh*3600)/60 - mod((restTime-hh*3600)/60,1);
ss= restTime-hh*3600-mm*60;



disp([...
    yourgrade '論文提出までの残り時間は…' num2str(int16(hh)) '時間' ...
    num2str(int16(mm)) '分' num2str(int16(ss)) '秒' ...
    
    ])
